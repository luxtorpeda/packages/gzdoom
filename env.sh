#!/bin/bash

# shellcheck disable=SC2155
export SOURCE_DATE_EPOCH=$(git log -1 --format=%ct)

export STEAM_APP_ID_LIST="2280 2290 2300 2360 2370 2390 9160"

source lib.sh
