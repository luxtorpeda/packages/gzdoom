#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_dist_dirs "$STEAM_APP_ID_LIST"

# build GZDoom
#
pushd "source"
mkdir -p build
cd build
cmake \
	-DCMAKE_BUILD_TYPE=Release \
	..
make -j "$(nproc)"
popd

for app_id in $STEAM_APP_ID_LIST ; do
	mkdir -p "$app_id/dist/license/"
	cp -rfv "source/build"/{gzdoom,soundfonts,*.pk3} "$app_id/dist"
	cp -v   "source/LICENSE"                         "$app_id/dist/license/LICENSE.gzdoom.txt"
done
